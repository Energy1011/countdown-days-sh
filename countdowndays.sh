#!/bin/bash

#CountdownDays script to calculate the number of days between dates excluding optional days of the week
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function usage() {
cat << EOF
Usage: $0 [OPTIONS] [ARGUMENTS]

    OPTIONS:
    -s  start date (if it is not given, default is current date)
    -e  end date, example 
    -h  show this help
    -x  exclude days, regrex string like "Sat|Sun" to exlude Saturdays and Sundays
    -v  turn on verbose mode


    EXAMPLES
    Get count from actual date to Mar 09 2017 excluding saturdays and sundays
    Example: $0 -e "Mar 09 2017" -x "Sun|Sat"

    Get count for all days from Jan 01 2017 to Dec 31 2017 
    Example: $0 -s "Jan 01 2017" -e "Dec 31 2017" 

EOF
}

opts='s:e:x:hv'
verbose=false

while getopts $opts option; do
    case $option in
        s) start_date=$OPTARG;;
        e) end_date=$OPTARG;;
        x) days_regex=$OPTARG;;
        h) usage; exit;;
        v) verbose=true;;
        \?) echo "Invalid option: -$OPTARG" >&2; exit 1;;
        :) echo "Missing argument for option: -$OPTARG" >&2; exit 1;;
        *) echo "Unknown option: -$OPTARG" >&2; exit 1;;
    esac
done

#If start_date wasn't set then current date
if [ -z	"$start_date" ]; then
	start_date=`date "+%b %d %Y"`
fi

if [ -z "$end_date" ]; then
	echo "-e option or argument missing: end date is needed.";
	usage;
	exit 1;
fi

if [ -z "$days_regex" ]; then
	days_regex="ZZZ"
fi

condition=true
step_days=1
days_counter=0
start_date=`echo $start_date | tr "[:upper:]" "[:lower:]"`
end_date=`echo $end_date | tr "[:upper:]" "[:lower:]"`
while $condition; do

    tmp_date=`date "+%a %b %d %Y" -d "$start_date +$step_days days"`
    if [[ ! $tmp_date =~ $days_regex ]]; then
        days_counter=$((days_counter+1))
	if $verbose ; then	
            echo "[+] $days_counter Counting this day" $tmp_date
        fi
    fi

    tmp_date=`date "+%b %d %Y" -d "$start_date +$step_days days"`
	tmp_date=`echo $tmp_date | tr "[:upper:]" "[:lower:]"`

    if [ "$end_date" == "$tmp_date" ]; then
        condition=false
    fi

    step_days=$((step_days+1))
   done

echo "[Result] $days_counter days from $start_date to $end_date"
if [[ $days_regex != "ZZZ" ]]; then
	echo "Excluding: $days_regex"
fi
