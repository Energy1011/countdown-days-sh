:date: CountDownDays bash script to calculate the number of days between dates excluding optional days of the week
=======

This script can be useful, for example to count how many days to your birthday or to know how many days have you lived since you born.

** I have to improve speed of this script **

Run it in a bash shell with:
$./countdowndays.sh 

Usage: $./countdowndays.sh [OPTIONS] [ARGUMENTS]

    OPTIONS:
    -s  start date (if it is not given, default is current date)
    -e  end date, example 
    -h  show this help
    -x  exclude days, regrex string like "Sat|Sun" to exlude saturdays and sundays
    -v  turn on verbose mode


    EXAMPLES
    Get count from actual date to Mar 09 2017 excluding saturdays and sundays
    Example: $./countdowndays.sh -e "Mar 09 2017" -x "Sun|Sat"

    Get count for all days from Jan 01 2017 to Dec 31 2017 
    Example: $./countdowndays.sh -s "Jan 01 2017" -e "Dec 31 2017" 
